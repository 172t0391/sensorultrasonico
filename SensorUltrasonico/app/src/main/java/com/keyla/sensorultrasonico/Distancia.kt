package com.keyla.sensorultrasonico

import androidx.annotation.Keep
import com.google.firebase.database.IgnoreExtraProperties

@Keep
@IgnoreExtraProperties
data class Distancia(var distan:String? = null, var date:String?  = null)