package com.keyla.sensorultrasonico

import android.annotation.SuppressLint
import android.content.Context
import android.util.MonthDisplayHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text
import java.util.*
import kotlin.collections.ArrayList




class AdapterDistancia(var list: ArrayList<Distancia>) : RecyclerView.Adapter<HolderDistancia>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderDistancia {

        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.item_recyler, parent, false)

        return HolderDistancia(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(v: HolderDistancia, pos: Int) {

        val HolderDistancia = v as HolderDistancia

        HolderDistancia.distancia.text = "Dato: " + list[pos].distan + " cm"
        HolderDistancia.fecha.text =  list[pos].date


    }
}