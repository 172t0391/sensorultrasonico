package com.keyla.sensorultrasonico

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_grafica.*
import kotlinx.android.synthetic.main.activity_main.*

class ActivityGrafica : AppCompatActivity() {
    private lateinit var database: FirebaseDatabase
    private lateinit var databaseReference: DatabaseReference
    private var dataset: ArrayList<Entry> = ArrayList()

    private var bandera = true
    private var i = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grafica)

        database = FirebaseDatabase.getInstance()
        databaseReference = database.getReference("Sensores")
      //  lista_distancia_Recycler.layoutManager = LinearLayoutManager(this)
        visualizandoDatos()

    }

    private fun visualizandoDatos(){
        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.e("cancel", error.toString())
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val list = ArrayList<Distancia>()
                var sizeData:Int = snapshot.childrenCount.toInt()
                for(data in snapshot.children){

                    val model = data.getValue(Distancia::class.java)
                    val dato1 = data.child("Distancia").value.toString()
                    print(dato1)

                    var entry = Entry(i.toFloat(), dato1.toFloat())

                    if (bandera) {
                        dataset.add(entry)

                        i++
                    }else if(i == sizeData) {
                        dataset.add(entry)

                        i++
                    }
                    graficando()
                  //  list.add(Distancia(dato1))
                }


            }
        })
    }
    private fun graficando() {
        var lineDataSet: LineDataSet = LineDataSet(dataset,"Distancia")
        //var lineDataSet: LineDataSet = LineDataSet(dataset,"Distancia")
        var iLineDataSets:ArrayList<ILineDataSet> = ArrayList()
        iLineDataSets.add(lineDataSet)

        var lineData: LineData = LineData(iLineDataSets)
        grafiquita.data = lineData
        grafiquita.invalidate()
        grafiquita.setNoDataText("Datos no disponibles")

        lineDataSet.color = Color.BLACK
        lineDataSet.setCircleColor(Color.YELLOW)
        lineDataSet.setDrawCircles(true)
        lineDataSet.lineWidth = 2f
        lineDataSet.circleRadius = 5f
        lineDataSet.valueTextSize = 10f
        lineDataSet.valueTextColor = Color.YELLOW
    }

}