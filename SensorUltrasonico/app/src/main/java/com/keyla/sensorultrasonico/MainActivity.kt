package com.keyla.sensorultrasonico

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*


//import com.google.firebase.QuerySnapshot
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    //private lateinit var rvLista: RecyclerView

    private lateinit var database: FirebaseDatabase
    private lateinit var databaseReference: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        database = FirebaseDatabase.getInstance()
        databaseReference = database.getReference("Sensores")
        lista_distancia_Recycler.layoutManager = LinearLayoutManager(this)
        lista_distancia_Recycler.setHasFixedSize(true)
       // val adapter = AdapterDistancia(this)
     //   lista_distancia_Recycler.adapter
        visualizandoDatos()
        btnGrafica.setOnClickListener {
            val intent = Intent(this,ActivityGrafica::class.java)
            startActivity(intent)
        }


    }

    private fun visualizandoDatos(){
        databaseReference.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Log.e("cancel", error.toString())
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val list = ArrayList<Distancia>()
                var sizeData:Int = snapshot.childrenCount.toInt()
                for(data in snapshot.children){

                    val model = data.getValue(Distancia::class.java)

                    val dato = data.child("Distancia").value.toString()
                    val fecha = data.child("Fecha").value.toString()

                    //print(dato)
                    list.add(Distancia(dato,fecha))
                }
                if(list.size > 0){
                    val adapter = AdapterDistancia(list)
                    adapter.notifyDataSetChanged()
                    lista_distancia_Recycler.smoothScrollToPosition(list.size)
                    lista_distancia_Recycler.adapter = adapter
                }

            }
        })
    }



}




